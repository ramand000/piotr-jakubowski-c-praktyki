﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Battery
    {
        private int power;
        private int? slot;

        // Constructor 0 arguments. Sets power to 0
        public Battery()
        {
            power = 0;
        }

        // Constructor 1 argument.
        public Battery(int pow)
        {
            power = pow;
        }

        public void setPower(int pow)
        {
            power = pow;
        }

        public int getPower()
        {
            return power;
        }

        public void setSlot(int s)
        {
            slot = s;
        }

        public int? getSlot()
        {
            return slot;
        }

        public override string ToString()
        {
            return ("Hi! I am a Battery. I have " + power + " power left ");
        }

    }
}
