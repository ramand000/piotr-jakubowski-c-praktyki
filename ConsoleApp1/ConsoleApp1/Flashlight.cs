﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Flashlight
    {
        private Bulb bulb;
        private Battery[] batteries = new Battery[4];
        private bool TurnedOn = false;

        //Constructor 0 arguments. Sets bulb and batteries to null
        public Flashlight()
        {

        }

        public string batteryStatus()
        {
            string s = "";
            for (int b = 0; b < 4; b++)
            {
                if (!(batteries[b] is null))
                    s += "Battery Slot " + (b + 1).ToString() + ": " + batteries[b] + Environment.NewLine;
                else
                    s += "Battery Slot " + (b + 1).ToString() + ": " + "There is no battery in here" + Environment.NewLine;
            }

            return s;
        }

        public string bulbStatus()
        {
            string s = "Bulb Slot: ";
            if (bulb is null)
                s += "There is no bulb in here";
            else
                s += bulb;
            return s + Environment.NewLine;
        }

        public string isTurnedOn()
        {
            string s;
            if (TurnedOn == true)
                s = "TurnedOn";
            else
                s = "TurnedOff";
            return s;
        }

        public string flashlightStatus()
        {
            string s = bulbStatus() + batteryStatus() + isTurnedOn();
            return s;
        }

        public void insertBattery(Battery battery)
        {
            bool inserted = false;
            if (!TurnedOn)
                for (int b = 0; b < batteries.Length & !inserted; b++)
                {
                    if (batteries[b] is null)
                    {
                        batteries[b] = battery;
                        inserted = true;
                    }

                }
            else
                Console.WriteLine("Oops Battery cannot be inserted ");
        }

        public void removeBattery(int batterySlot)
        {
            if (!TurnedOn)
                batteries[batterySlot - 1] = null;
            else
                Console.WriteLine("Oops Battery cannot be removed ");
        }

        public void insertBulb(Bulb insertedbulb)
        {
            if (bulb is null)
                bulb = insertedbulb;
            else
                Console.WriteLine("Oops Bulb cannot be inserted");
        }

        public void changeBulb(Bulb insertedbulb)
        {
            if (!(bulb is null) & !TurnedOn)
                bulb = insertedbulb;
            else
                Console.WriteLine("Oops Bulb cannot be changed");
        }

        public int AmountOfBatteries()
        {
            int x = 0;
            foreach (Battery b in batteries)
            {
                if (!(b is null))
                    x += 1;
            }
            return x;
        }

        public int getLowestBatteryPower()
        {
            int Lpower = 100;
            foreach (Battery b in batteries)
            {
                if (!(b is null))
                    if (b.getPower() < Lpower)
                        Lpower = b.getPower();
            }
            return Lpower;
        }

        public void TurnOn()
        {
            if (getLowestBatteryPower() >= 15 & !(bulb is null) & AmountOfBatteries() >= 2 & !TurnedOn)
            {
                Console.WriteLine("I am flashing my light. My power levels drop");
                TurnedOn = true;
                foreach (Battery b in batteries)
                    if (!(b is null))
                        b.setPower(b.getPower() - bulb.getBrightness());
                Console.ReadKey();
            }
            else Console.WriteLine("Something went wrong. Maybe there is not enough batteries or they have lost their power, maybe I am already Turned On");
        }

        public void TurnOff()
        {
            TurnedOn = false;
            Console.WriteLine("Turned Off");
        }

        public override string ToString()
        {
            if (AmountOfBatteries() is 0 & bulb is null)
                return ("Hey I am a Flashlight." + Environment.NewLine + isTurnedOn());
            else
                return ("Hey I am a Flashlight. With me are those guys:" + Environment.NewLine + flashlightStatus());
        }

    }
}
