﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Bulb
    {
        private int brightness;

        // Constructor 0 arguments. Sets brightness to 1
        public Bulb()
        {
            brightness = 1;
        }

        public Bulb(int bright)
        {
            brightness = bright;
        }

        public void setBrightness(int bright)
        {
            brightness = bright;
        }

        public int getBrightness()
        {
            return brightness;
        }

        public override string ToString()
        {
            return ("I am a Bulb. Strength of my light is " + brightness + " on a scale from 1 to 10 ");
        }

    }
}
