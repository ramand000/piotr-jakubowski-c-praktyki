﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ConsoleApp1.Battery;
using static ConsoleApp1.Bulb;
using static ConsoleApp1.Flashlight;

namespace ConsoleApp1
{
    public class Menu
    {
        public static int clump(int value, int max, int min)
        {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        } 



        public static int makeMenu(string[] menuItems, Flashlight flashlight)
        {
            int selected = 0;
            while (true)
            {
                for (int i = 0; i < menuItems.Length; i++)
                {
                    if (selected == i)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(menuItems[i]);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine(menuItems[i]);
                    }
                }
                Console.WriteLine(Environment.NewLine);

                ConsoleKeyInfo key = Console.ReadKey();

                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        if ((selected - 1) < 0)
                        {
                            selected = menuItems.Length - 1;
                        }
                        else
                        {
                            selected--;
                        }
                        break;

                    case ConsoleKey.DownArrow:
                        if ((selected + 1) > menuItems.Length - 1)
                        {
                            selected = 0;
                        }
                        else
                        {
                            selected++;
                        }
                        break;

                    case ConsoleKey.Enter:
                        switch (selected + 1)
                        {
                            case 1:
                                flashlight.TurnOn();
                                break;

                            case 2:
                                flashlight.TurnOff();
                                break;

                            case 3:
                                Console.WriteLine("The power of this battery is(range is 1 - 100, in case of exceeding it will round to the closest extremum): ");
                                int pow = 0;
                                try
                                {
                                    pow = Convert.ToInt32(Console.ReadLine());
                                    pow = clump(pow, 100, 1);
                                    flashlight.insertBattery(new Battery(pow));
                                }
                                catch(Exception)
                                {
                                    Console.WriteLine("Please use NUMBERS from 1 to 100");
                                }
                                break;

                            case 4:
                                Console.WriteLine("The slot from which u want to remove battery(range is 1 - 4, in case of exceeding it will round to the closest extremum): ");
                                int slot = 0;
                                try
                                {
                                    slot = Convert.ToInt32(Console.ReadLine());
                                    slot = clump(slot, 4, 1);
                                    flashlight.removeBattery(slot);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Please use NUMBERS from 1 to 4");
                                }
                                break;

                            case 5:
                                Console.WriteLine("The power of this Bulbs light is(range is 1 - 10, in case of exceeding it will round to the closest extremum): ");
                                int bright = 0;
                                try
                                {
                                    bright = Convert.ToInt32(Console.ReadLine());
                                    bright = clump(bright, 10, 1);
                                    flashlight.insertBulb(new Bulb(bright));
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Please use NUMBERS from 1 to 10");
                                }
                                break;

                            case 6:
                                Console.WriteLine("The power of this Bulbs light is(range is 1 - 10, in case of exceeding it will round to the closest extremum): ");
                                int brightn = 0;
                                try
                                {
                                    brightn = Convert.ToInt32(Console.ReadLine());
                                    brightn = clump(brightn, 10, 1);
                                    flashlight.changeBulb(new Bulb(brightn));
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Please use NUMBERS from 1 to 10");
                                }
                                break;

                            case 7:
                                Console.WriteLine(flashlight);
                                break;
                        }
                        Console.ReadKey();
                        break;


                }
                Console.Clear();
            }
        }

    }
}
