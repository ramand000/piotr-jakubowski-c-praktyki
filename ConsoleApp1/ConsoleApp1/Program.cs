﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ConsoleApp1.Battery;
using static ConsoleApp1.Bulb;
using static ConsoleApp1.Flashlight;
using static ConsoleApp1.Menu;

namespace ConsoleApp1
{



    class Program
    {
        static void Main(string[] args)
        {
            Flashlight flashlight = new Flashlight();

            makeMenu(new string[]{
            "Turn Flashlight On",
            "Turn Flashlight Off",
            "Insert Battery",
            "Remove Battery",
            "Insert Bulb",
            "Change Bulb",
            "Check How Is Your Flashlight",
            }, flashlight);
            
        }
    }
}
